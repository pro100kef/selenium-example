import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){
        System.setProperty("webdriver.chrome.driver","C:/CrashCourseProjects/selenium-example/src/main/resources/opera_driver.exe");
        WebDriverManager.operadriver().setup();
        WebDriver driver = new OperaDriver();
        driver.manage().window().maximize();
        driver.manage().window().setSize(new Dimension(1920, 1080));
        driver.get("https://www.google.com.ua/?hl=ru");

        JavascriptExecutor jsq = (JavascriptExecutor) driver;
        jsq.executeScript("window.open()");
        List<String> wh = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(wh.get(1));
        driver.navigate().to("https://demo.opencart.com/index.php?route=common/home");
        driver.close();
        driver.switchTo().window(wh.get(0));

        /* WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("MacBook");
        element.sendKeys(Keys.ENTER);
        List<WebElement> adds = driver.findElements(By.xpath("//h3"));
        for (WebElement e:adds) {
            if(e.getText().contains("HOTLINE")){
                e.click();
                break;
            }
        } */

        driver.close();
        System.out.println(driver);
        driver.quit();
        System.out.println(driver);
    }
}
